package com.wipro.microservices.AMessageQueuingP;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@AllArgsConstructor
public class RabbitMQMessageProducer {

	private final AmqpTemplate amqpTemplate;

	public void publish(final Object payload, final String exchange, final String routingKey) {
		log.info("Publishing to {} using routingKey {}. Payload: {}", exchange, routingKey, payload);
		amqpTemplate.convertAndSend(exchange, routingKey, payload);
		log.info("Published to {} using routingKey {}. Payload: {}", exchange, routingKey, payload);
	}

}
