package com.wipro.microservices.Customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = { "com.wipro.microservices.Customer",
		"com.wipro.microservices.AMessageQueuingP", })
@EnableEurekaClient
@EnableFeignClients(basePackages = "com.wipro.microservices.Clients")
//@PropertySources({ @PropertySource("classpath:clients-${spring.profiles.active}.properties") })
public class CustomerApp {
	public static void main(final String[] args) {
		SpringApplication.run(CustomerApp.class, args);
	}

}
