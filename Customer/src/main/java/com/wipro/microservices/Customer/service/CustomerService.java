package com.wipro.microservices.Customer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.wipro.microservices.AMessageQueuingP.RabbitMQMessageProducer;
import com.wipro.microservices.Clients.fraud.FraudCheckResponse;
import com.wipro.microservices.Clients.fraud.FraudClient;
import com.wipro.microservices.Clients.notification.NotificationRequest;
import com.wipro.microservices.Customer.entity.Customer;
import com.wipro.microservices.Customer.repository.CustomerRepository;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private FraudClient fraudClient;
	@Autowired
	private RabbitMQMessageProducer rabbitMQMessageProducer;

	public ResponseEntity<Customer> registerCustomer(final Customer customer) {

		Customer customer2 = Customer.builder().firstName(customer.getFirstName()).lastName(customer.getLastName())
				.email(customer.getEmail()).build();
		// todo: check if email valid
		// todo: check if email not taken
		customer2 = customerRepository.saveAndFlush(customer2);

		final FraudCheckResponse fraudCheckResponse = fraudClient.isFraudster(customer.getId());

		if (fraudCheckResponse.isFraud())
			throw new IllegalStateException("fraudster");

		final NotificationRequest notificationRequest = new NotificationRequest(customer.getId(), customer.getEmail(),
				String.format("Hi %s, welcome ...", customer.getFirstName()));
		rabbitMQMessageProducer.publish(notificationRequest, "internal.exchange", "internal.notification.routing-key");

		// final Customer customer2 = customerRepository.save(customer);

		return new ResponseEntity<>(customer2, HttpStatus.CREATED);
	}

	public ResponseEntity<Customer> getCustomerById(final Integer id) {
		final Customer customer = customerRepository.findById(id).orElseThrow(null);

		return new ResponseEntity<>(customer, HttpStatus.FOUND);
	}

}
