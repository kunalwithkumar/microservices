package com.wipro.microservices.Notification.service;

import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

import com.wipro.microservices.Clients.notification.NotificationRequest;
import com.wipro.microservices.Notification.entity.Notification;
import com.wipro.microservices.Notification.repository.NotificationRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class NotificationService {

	private final NotificationRepository notificationRepository;

	public void send(final NotificationRequest notificationRequest) {
		notificationRepository.save(Notification.builder().toCustomerId(notificationRequest.toCustomerId())
				.toCustomerEmail(notificationRequest.toCustomerName()).sender("Amigoscode")
				.message(notificationRequest.message()).sentAt(LocalDateTime.now()).build());
	}

}
