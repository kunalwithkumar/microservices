package com.wipro.microservices.Notification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
//@SpringBootApplication(
//        scanBasePackages = {
//                "com.amigoscode.notification",
//                "com.amigoscode.amqp",
//        }
//)
//@PropertySources({
//        @PropertySource("classpath:clients-${spring.profiles.active}.properties")
//})
@EnableEurekaClient
public class NotificationApp {
	public static void main(final String[] args) {
		SpringApplication.run(NotificationApp.class, args);
	}
}