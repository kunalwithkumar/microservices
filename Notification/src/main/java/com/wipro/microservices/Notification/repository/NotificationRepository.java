package com.wipro.microservices.Notification.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.microservices.Notification.entity.Notification;

public interface NotificationRepository extends JpaRepository<Notification, Integer> {

}
