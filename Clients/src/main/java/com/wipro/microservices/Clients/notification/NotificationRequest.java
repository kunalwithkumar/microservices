package com.wipro.microservices.Clients.notification;

public record NotificationRequest(Integer toCustomerId, String toCustomerName, String message) {
}