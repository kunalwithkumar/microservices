package com.wipro.microservices.Clients.fraud;

public record FraudCheckResponse(boolean isFraud) {
}
