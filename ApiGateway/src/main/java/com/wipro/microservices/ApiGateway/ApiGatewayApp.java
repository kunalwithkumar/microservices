package com.wipro.microservices.ApiGateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ApiGatewayApp {
	public static void main(final String[] args) {
		SpringApplication.run(ApiGatewayApp.class, args);
	}
}