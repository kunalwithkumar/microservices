package com.wipro.microservices.fraud.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.microservices.fraud.entity.FraudCheckHistory;
import com.wipro.microservices.fraud.repository.FraudCheckHistoryRepository;

@Service
public class FraudCheckService {

	@Autowired
	private FraudCheckHistoryRepository fraudCheckHistoryRepository;

	public boolean isFraudulentCustomer(final Integer customerId) {
		fraudCheckHistoryRepository.save(FraudCheckHistory.builder().customerId(customerId).isFraudster(false)
				.createdAt(LocalDateTime.now()).build());
		return false;
	}

}
