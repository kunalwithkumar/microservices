package com.wipro.microservices.fraud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.microservices.Clients.fraud.FraudCheckResponse;
import com.wipro.microservices.fraud.service.FraudCheckService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/fraud")
@Slf4j
public class FraudController {

	@Autowired
	private FraudCheckService fraudCheckService;

	@GetMapping(path = "{customerId}")
	public FraudCheckResponse isFraudster(@PathVariable("customerId") final Integer customerId) {

		final boolean isFraudulentCustomer = fraudCheckService.isFraudulentCustomer(customerId);
		log.info("fraud check request for customer {}", customerId);

		return new FraudCheckResponse(isFraudulentCustomer);
	}

}
