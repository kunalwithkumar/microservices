package com.wipro.microservices.fraud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
//@PropertySources({ @PropertySource("classpath:clients-${spring.profiles.active}.properties") })
public class FraudApp {
	public static void main(final String[] args) {
		SpringApplication.run(FraudApp.class, args);
	}
}