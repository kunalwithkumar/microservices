package com.wipro.microservices.EurekaServerMicroServices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaServerMicroServicesApp {
	public static void main(final String[] args) {
		SpringApplication.run(EurekaServerMicroServicesApp.class, args);
	}
}